const data=require('./countries.json')
const fs = require('fs')

let obj={};
for(let i in data){
    if(obj[data[i].region]===undefined){
        obj[data[i].region]={};
        obj[data[i].region][data[i].subregion]={
            populations:data[i].population,
            count:1,
        };

    }
    else{
        if(obj[data[i].region][data[i].subregion]===undefined){
            obj[data[i].region][data[i].subregion]={
                populations:data[i].population,
                count:1,
            }
          obj[data[i].region][data[i].subregion].populations += ( Number(data[i].population));
          obj[data[i].region][data[i].subregion].count=1;
      }
      else{
          obj[data[i].region][data[i].subregion].populations+= (Number(data[i].population));
          obj[data[i].region][data[i].subregion].count++;
      }
  }
}
for(let i in obj){
    for(let j in obj[i]){
        obj[i][j]['Average']=Number((obj[i][j].populations/obj[i][j].count).toFixed(2));
    }

}
 fs.writeFileSync("6.OUTPUT_Average_population_of_every_subregion_in_a_region.json",JSON.stringify(obj),'utf8',function(err){console.log(err);});

console.log(obj);