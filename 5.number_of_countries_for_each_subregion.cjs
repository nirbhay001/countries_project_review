const data=require('./countries.json')

let obj={};
for(let i in data){
    let region=data[i].region;
    let subregion=data[i].subregion;

    if(obj[data[i].region]===undefined){
        obj[region]={
        };
        obj[region][subregion]=1;

    }
    else{
          if(obj[data[i].region][data[i].subregion]===undefined){
            if(data[i].subregion===undefined){
                continue;
            }
            obj[data[i].region][data[i].subregion]=1;
        }
        else{
            obj[data[i].region][data[i].subregion]++;
        }
    }
}
console.log(obj);