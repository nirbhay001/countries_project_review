const data=require('./countries.json');

let obj={};
for(let i in data){
    obj[data[i].name.official]=data[i].area;
}
const sortdata= Object.entries(obj).sort((a,b)=>{
    return b[1]-a[1];
}).slice(0,5);
console.log(Object.fromEntries(sortdata));
